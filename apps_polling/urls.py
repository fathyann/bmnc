from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    url(r'^$', polling_page, name='polling-page'),
    url(r'^detail/$', polling_detail, name='polling-detail'),
]
