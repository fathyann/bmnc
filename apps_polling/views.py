from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

# Create your views here.
response={}

def polling_page(request):
    response['hal_polling'] = "Disini halaman polling"
    return render(request, 'polling_page.html', response)

def polling_detail(request):
    response['detail_polling'] = "Disini halaman detail polling"
    return render(request, 'polling_detail.html', response)
