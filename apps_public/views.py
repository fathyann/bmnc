from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

# Create your views here.
response={}

def news_page(request):
    response['news_page'] = "Disini halaman home/news"
    return render(request, 'news_page.html', response)

def news_detail(request):
    response['detail_news'] = "Disini halaman detail news berisi komentar dll"
    return render(request, 'news_detail.html', response)
