from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    url(r'^$', news_page, name='news-page'),
    url(r'^detail/$', news_detail, name='news-detail'),
]
