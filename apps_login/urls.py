from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    url(r'^$', login_page, name='login'),
    url(r'^register/$', register_page, name='register'),
]
