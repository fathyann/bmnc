from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

# Create your views here.
response={}

def login_page(request):
    response['tes'] = "Tes"
    return render(request, 'login.html', response)

def register_page(request):
    response['cek'] = "Halaman register"
    return render(request, 'register.html', response)
