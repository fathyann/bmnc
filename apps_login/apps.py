from django.apps import AppConfig


class AppsLoginConfig(AppConfig):
    name = 'apps_login'
