from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    url(r'^edit-profile/$', edit_profile, name='edit-profile'),
    url(r'^user-profile/$', show_profile, name='user-profile'),
]
