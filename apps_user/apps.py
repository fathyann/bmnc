from django.apps import AppConfig


class AppsUserConfig(AppConfig):
    name = 'apps_user'
