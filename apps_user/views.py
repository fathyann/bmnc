from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

# Create your views here.
response={}

def edit_profile(request):
    response['edit'] = "Disini halaman edit profil user"
    return render(request, 'edit_profile.html', response)

def show_profile(request):
    response['show'] = "Disini halaman user profile"
    return render(request, 'profile_page.html', response)
